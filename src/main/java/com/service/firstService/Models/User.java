package com.service.firstService.Models;

public class User {
	
	private String name ;
	private String gender;
	private String userName;
	private String password;
	
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public User()
	{
		this.userName="admin@me";
		this.password="123456";
	}
	
	
	
	public User(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}
	public User(String userName, String password,String gender,String name) {
		
		this.userName = userName;
		this.password = password;
		this.gender=gender;
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
