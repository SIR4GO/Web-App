package com.service.firstService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("page1")
public class Page1 
{
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public String auth(@Context HttpServletRequest req)
	{
		
		try
		{
			HttpSession session=req.getSession();
			int user_id=(int)session.getAttribute("user_Id");
			if(user_id==1)
				return "Page1";
		}
		catch(Exception e)
		{
			return "error";
		}
		
		return "Not";
		
	}
}
