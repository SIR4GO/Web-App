package com.service.firstService.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	 private String url = "jdbc:mysql://localhost:3306/app";
	 private String username = "root";
	 private String password = "";
	 private Connection connection;

	
	
	public Connection getConnection () throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		  try {
	  		     
			  Class.forName("com.mysql.jdbc.Driver").newInstance();
			  connection =  DriverManager.getConnection(url, username, password);
			  
			  return connection;
		  } catch (SQLException e1) {
			e1.printStackTrace();
		}
		
	  
		
		
		return null;
	}

}
