package com.service.firstService.Config;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class SingletonDB {

	private static SingletonDB DB;
	private final String url = "jdbc:mysql://localhost:3306/app";
	private final String username = "root";
	private final String password = "";
	private Connection connection;

	private SingletonDB() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		System.out.println("Connecting database...");
   	    try {
   		   
   		       // DriverManager.registerDriver(new com.mysql.jdbc.Driver ());  
			
   		   
				 Class.forName("com.mysql.jdbc.Driver").newInstance();
			
   		  connection =  DriverManager.getConnection(url, username, password);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
   	
   	  
		
   	
	}

	public static SingletonDB getDB() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (DB == null) {
			DB = new SingletonDB();
		}
		System.out.println("s");
		return DB;
	}

	public  Connection getConnection() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (DB == null) {
			DB = new SingletonDB();
		}
		return DB.connection;
	}
}