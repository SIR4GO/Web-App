package com.service.firstService;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserInfo {
	
	private String userName;
	private String message;
	private String pageUrl;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	
	
	

}
