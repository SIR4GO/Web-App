package com.service.firstService;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Weather {

	private String temp;
	private String weatherStatue;
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	public String getWeatherStatue() {
		return weatherStatue;
	}
	public void setWeatherStatue(String weatherStatue) {
		this.weatherStatue = weatherStatue;
	}
	
}
