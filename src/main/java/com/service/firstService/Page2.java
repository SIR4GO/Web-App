package com.service.firstService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("page2")
public class Page2 
{
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public String auth(@Context HttpServletRequest req)
	{
		HttpSession session=req.getSession();
		int user_id=(int)session.getAttribute("user_Id");
		if(user_id==2)
			return "Page2";
		return "Not Have Access";
		
		
	}
}
