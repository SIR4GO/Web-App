<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="Style.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

</head>
<body>

<!--  
<form id="contactForm1">
  <input name="userName" id="username" type="text">
  <input name="password" id ="password" type="text">
  <button type="button" id="sendAjax">Login</button>
</form>
-->
<div class='form'>
    <h1>Login Page</h1>
  <form id="contactForm1" method="POST" >
      <div class="inputWithIcon">
          <input id="username" name="userName" type="text" placeholder="Username" required>
          <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
      </div>
      <div class="inputWithIcon">
          <input id="password" name="password" type="password" placeholder="Password" required>
          <i class="fa fa-unlock fa-lg fa-fw" aria-hidden="true"></i>
      </div>
      <button type="button" id="sendAjax">Login</button>
  </form>
</div>

</body>


<script>

$('#sendAjax').on('click',function(){

	var userName = $("#username").val();
	var password = $("#password").val();
	$.ajax('http://localhost:8080/firstService/login',{
			'data': JSON.stringify({"userName":userName,"password":password}), //{action:'x',params:['a','b','c']}
			'type': 'POST',
			'processData': false,
			'contentType': 'application/json' 
			//typically 'application/x-www-form-urlencoded', but the service you are calling may expect 'text/json'... check with the service to see what they expect as content-type in the HTTP header.
		}).done(function(response,status){
			console.log(status);
		}).fail(function(response,status){
			console.log(status);
		});
	
});
/*var frm = $('#contactForm1');
$(document).ready(function(){
    frm.submit(function (e){
	
		$.ajax('http://localhost:8080/firstService/login',{
			'data': JSON.stringify({userName:"omar@me",password:"123456"}), //{action:'x',params:['a','b','c']}
			'type': 'POST',
			'processData': false,
			'contentType': 'application/json' 
			//typically 'application/x-www-form-urlencoded', but the service you are calling may expect 'text/json'... check with the service to see what they expect as content-type in the HTTP header.
		});
        $.ajax({type: post, url: "http://localhost:8080/firstService/login",data: frm.serialize()  , success: function(result){
              console.log("submit success");
			  console.log(data)
        }});
    });
});*/
</script>
</html>
